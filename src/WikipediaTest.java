import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

class WikipediaTest
{
    private static final String testString = "https://jsonplaceholder.typicode.com/albums";

    private static final String constantTestString = "1 quidem molestiae enim 1";

    @org.junit.jupiter.api.Test
    void parseTest() throws IOException
    {
        URL url = new URL(testString);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoOutput(true);
        StringBuilder response = new StringBuilder();
        BufferedReader in = new BufferedReader
                (
                        new InputStreamReader(connection.getInputStream()));
        String line;

        while ((line = in.readLine()) != null)
        {
            response.append(line);
        }
        in.close();

        String stringInJson = response.toString();

        JSONArray array = new JSONArray(stringInJson);

        String a = null;
        for (int i = 0; i < array.length()-99; i++)
        {
            JSONObject object = array.getJSONObject(i);
            int id = object.getInt("id");
            int userId = object.getInt("userId");
            String title = object.getString("title");
            System.out.println(id + " " + title + " " + userId);
            a = id + " " + title + " " + userId;
        }
        System.out.println();

        Assertions.assertEquals(a, constantTestString);
    }
}