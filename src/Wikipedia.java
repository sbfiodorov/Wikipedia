import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.awt.*;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Wikipedia
{
    private static final ArrayList<String> list = new ArrayList<>();
    public static String search () throws Exception
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите запрос : ");
        String str = scanner.next();
        String host = "https://ru.wikipedia.org/w/api.php?action=query&list=search&utf8=&format=json&srsearch=";

        String encoded_query = URLEncoder.encode(str, StandardCharsets.UTF_8);
        URL url = new URL(host + encoded_query);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoOutput(true);
        StringBuilder response = new StringBuilder();
        BufferedReader in = new BufferedReader
                (
                new InputStreamReader(connection.getInputStream()));
        String line;

        while ((line = in.readLine()) != null)
        {
            response.append(line);
        }
        in.close();

        return response.toString();
    }
    public static void parse(String str) throws IOException
    {
        try
        {
            int startIndex = str.indexOf("[");
            int endIndex = str.lastIndexOf("}}");

            String endString = str.substring(startIndex, endIndex);

            JSONArray array = new JSONArray(endString);

            for (int i = 0; i < array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                int pageId = object.getInt("pageid");
                String title = object.getString("title");
                String a = "https://ru.wikipedia.org/w/index.php?curid=" + pageId;
                System.out.println(i + " : " + title);
                list.add(a);
            }
            System.out.println();

            Desktop desktop = Desktop.getDesktop();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Выберите страницу по индексу : ");
            int result = scanner.nextInt();
            desktop.browse(URI.create(list.get(result)));
        }

        catch (IndexOutOfBoundsException e)
        {
            System.out.println("Введите значение индекса из списка");
        }
    }

    public static String prettify (String json_text)
    {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(json_text).getAsJsonObject();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(json);
    }

    public static void main(String[] args) throws Exception
    {
        String response = search();
        System.out.println();
        parse(response);
    }
}